#  This file is part of Lazylibrarian.
#
# Purpose:
#   Testing the startup sequence

from lazylibrarian.config2 import CONFIG
from lazylibrarian.notifiers import APPRISE_VER
from unittests.unittesthelpers import LLTestCase


class SetupTest(LLTestCase):

    def testConfig(self):
        # Validate that basic global objects and configs have run
        self.assertIsNotNone(CONFIG)
        self.assertIsInstance(CONFIG.get_int('LOGLIMIT'), int)

    def testApprise(self):
        # Validate that APPRISE is defined properly; it's set up as a global in notifiers,
        # copied from the value received during load in apprise_notify. This allows us to
        # avoid circular dependencies.
        self.assertIsNotNone(APPRISE_VER)

